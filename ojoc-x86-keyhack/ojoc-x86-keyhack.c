#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "decrypt.h"


int main(int argc, char *argv[]) {
    unsigned int offs[CLIENT_SECRET_SIZE];
    int len[CLIENT_SECRET_SIZE];
    int cnt  = 0;
    int sum  = 0;
    int rv;

    if (argc != 2 || strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        (void) printf("Usage: %s /path/to/libhmac.so | -h, --help\n\n"
                      "         -h, --help      Display this message\n", argv[0]);
        return -1;
    }

    /* We read from stdin a table of offsets and sizes where the
       different parts of the key are stored. This implemenetation
       assumes that the locations in the decompiled code are in ascending
       order, i.e., the location of byte 0 is specified first, then
       1 through 40. The input can be extracted easily in 4.24.2
       from the decompiled (retdec) x86 libhmac.so:

    *(int32_t *)(g4 + 200) = *(int32_t *)(g4 - 0x5377);
    *(char *)(g4 + 204) = *(char *)(g4 - 0x5373);
    *(int32_t *)(g4 + 205) = *(int32_t *)(g4 - 0x5368);
    *(char *)(g4 + 209) = *(char *)(g4 - 0x5364);
    *(int32_t *)(g4 + 210) = *(int32_t *)(g4 - 0x536d);
    [...]

    */


    /* Example:
        0x5377			4
        0x5373			1
        0x5368			4
        0x5364			1
        0x536d			4
        [...]
     */

    /* Read the offset as hexadecimal number, and then the size,
       build an array (list) of offsets. The sum of the sizes must
       equal 40, which is the length of the client secret. */
    

    while((rv = scanf("%x %d",&offs[cnt],&len[cnt])) != EOF && cnt < CLIENT_SECRET_SIZE) {
        assert(rv == 2);
        sum += len[cnt];
        cnt++;
    }

    assert(sum == CLIENT_SECRET_SIZE);
    FILE *fp = fopen(argv[1], "r");
    assert(fp != NULL);

    uint8_t arr[CLIENT_SECRET_SIZE];

    for (int base = KEY_LOCATION_START; base < KEY_LOCATION_START+KEY_LOCATION_HAYSTACK_SIZE; base++) {
        int bb = base + offs[0];
        int arro = 0;
        for(int i = 0; i < cnt; i++) {
            fseek(fp,bb-offs[i],SEEK_SET);
            for(int j = 0; j < len[i]; j++) {
                arr[arro++] = fgetc(fp);
            }
        }
        decode(arr);
    }

    fclose(fp);

    return 0;
}
